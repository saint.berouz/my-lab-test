<?php

namespace MaisonDigitale\Controllers;

class Controller {

    protected $container;

    public function __constructor($container){
        $this->container = $container;
    }
}